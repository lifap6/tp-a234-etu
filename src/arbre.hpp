#ifndef A234_ARBRE_HPP
#define A234_ARBRE_HPP

#include "noeud.hpp"

#include <iostream>

struct Arbre {
  Arbre() ;
  ~Arbre() ;

  void inserer(int v) ;
  Noeud* rechercher(int v) ;

  Noeud* racine ;
} ;

std::ostream& operator<<(std::ostream& out, Arbre& arbre) ;

#endif
