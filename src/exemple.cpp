#include "exemple.hpp"


Noeud* exemple_eclatement() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 20 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 3 ;
  n4->valeurs[0] = 10 ;
  n4->valeurs[1] = 12 ;
  n4->valeurs[2] = 14 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 3 ;
  n5->valeurs[0] = 30 ;
  n5->valeurs[1] = 32 ;
  n5->valeurs[2] = 34 ;
  n1->enfants[1] = n5 ;
  n1->enfants[2] = nullptr ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_eclatement_1() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 12 ;
  n1->valeurs[1] = 20 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 10 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 14 ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 3 ;
  n6->valeurs[0] = 30 ;
  n6->valeurs[1] = 32 ;
  n6->valeurs[2] = 34 ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_eclatement_2() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 3 ;
  n1->valeurs[0] = 12 ;
  n1->valeurs[1] = 20 ;
  n1->valeurs[2] = 32 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 10 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 14 ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 1 ;
  n6->valeurs[0] = 30 ;
  n1->enfants[2] = n6 ;
  Noeud* n7 = new Noeud(0) ;
  n7->taille = 1 ;
  n7->valeurs[0] = 34 ;
  n1->enfants[3] = n7 ;  

  return n1 ;
}

Noeud* solution_insertion_1() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 45 ;

  return n1 ;
}

Noeud* solution_insertion_2() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 6 ;
  n1->valeurs[1] = 45 ;

  return n1 ;
}

Noeud* solution_insertion_3() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 3 ;
  n1->valeurs[0] = 6 ;
  n1->valeurs[1] = 45 ;
  n1->valeurs[2] = 75 ;

  return n1 ;
}

Noeud* solution_insertion_4() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 45 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 24 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 75 ;
  n1->enfants[1] = n5 ;
  n1->enfants[2] = nullptr ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_5() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 45 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 24 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 73 ;
  n5->valeurs[1] = 75 ;
  n1->enfants[1] = n5 ;
  n1->enfants[2] = nullptr ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_6() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 45 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 24 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 3 ;
  n5->valeurs[0] = 73 ;
  n5->valeurs[1] = 75 ;
  n5->valeurs[2] = 78 ;
  n1->enfants[1] = n5 ;
  n1->enfants[2] = nullptr ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_7() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 24 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 59 ;
  n5->valeurs[1] = 73 ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 1 ;
  n6->valeurs[0] = 78 ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_8() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 24 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 59 ;
  n5->valeurs[1] = 73 ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 78 ;
  n6->valeurs[1] = 92 ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_9() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 24 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 59 ;
  n5->valeurs[1] = 73 ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 3 ;
  n6->valeurs[0] = 78 ;
  n6->valeurs[1] = 92 ;
  n6->valeurs[2] = 93 ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_10() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 3 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 10 ;
  n4->valeurs[2] = 24 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 59 ;
  n5->valeurs[1] = 73 ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 3 ;
  n6->valeurs[0] = 78 ;
  n6->valeurs[1] = 92 ;
  n6->valeurs[2] = 93 ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_11() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 3 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 10 ;
  n4->valeurs[2] = 24 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 3 ;
  n5->valeurs[0] = 59 ;
  n5->valeurs[1] = 67 ;
  n5->valeurs[2] = 73 ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 3 ;
  n6->valeurs[0] = 78 ;
  n6->valeurs[1] = 92 ;
  n6->valeurs[2] = 93 ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_12() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 3 ;
  n1->valeurs[0] = 10 ;
  n1->valeurs[1] = 45 ;
  n1->valeurs[2] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 6 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 24 ;
  n5->valeurs[1] = 44 ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 3 ;
  n6->valeurs[0] = 59 ;
  n6->valeurs[1] = 67 ;
  n6->valeurs[2] = 73 ;
  n1->enfants[2] = n6 ;
  Noeud* n7 = new Noeud(0) ;
  n7->taille = 3 ;
  n7->valeurs[0] = 78 ;
  n7->valeurs[1] = 92 ;
  n7->valeurs[2] = 93 ;
  n1->enfants[3] = n7 ;

  return n1 ;
}

Noeud* solution_insertion_13() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 45 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 10 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 44 ;
  n4->enfants[1] = n17 ;
  n4->enfants[2] = nullptr ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 75 ;
  n5->valeurs[1] = 92 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 3 ;
  n20->valeurs[0] = 59 ;
  n20->valeurs[1] = 67 ;
  n20->valeurs[2] = 73 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 1 ;
  n21->valeurs[0] = 78 ;
  n5->enfants[1] = n21 ;
  Noeud* n22 = new Noeud(0) ;
  n22->taille = 2 ;
  n22->valeurs[0] = 93 ;
  n22->valeurs[1] = 97 ;
  n5->enfants[2] = n22 ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  n1->enfants[2] = nullptr ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_14() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 45 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 10 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 44 ;
  n4->enfants[1] = n17 ;
  n4->enfants[2] = nullptr ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 75 ;
  n5->valeurs[1] = 92 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 3 ;
  n20->valeurs[0] = 59 ;
  n20->valeurs[1] = 67 ;
  n20->valeurs[2] = 73 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 78 ;
  n21->valeurs[1] = 82 ;
  n5->enfants[1] = n21 ;
  Noeud* n22 = new Noeud(0) ;
  n22->taille = 2 ;
  n22->valeurs[0] = 93 ;
  n22->valeurs[1] = 97 ;
  n5->enfants[2] = n22 ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  n1->enfants[2] = nullptr ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_15() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 45 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 10 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 44 ;
  n4->enfants[1] = n17 ;
  n4->enfants[2] = nullptr ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 3 ;
  n5->valeurs[0] = 67 ;
  n5->valeurs[1] = 75 ;
  n5->valeurs[2] = 92 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 1 ;
  n20->valeurs[0] = 59 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n5->enfants[1] = n21 ;
  Noeud* n22 = new Noeud(0) ;
  n22->taille = 2 ;
  n22->valeurs[0] = 78 ;
  n22->valeurs[1] = 82 ;
  n5->enfants[2] = n22 ;
  Noeud* n23 = new Noeud(0) ;
  n23->taille = 2 ;
  n23->valeurs[0] = 93 ;
  n23->valeurs[1] = 97 ;
  n5->enfants[3] = n23 ;
  n1->enfants[1] = n5 ;
  n1->enfants[2] = nullptr ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_16() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 45 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 10 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 3 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 28 ;
  n17->valeurs[2] = 44 ;
  n4->enfants[1] = n17 ;
  n4->enfants[2] = nullptr ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 3 ;
  n5->valeurs[0] = 67 ;
  n5->valeurs[1] = 75 ;
  n5->valeurs[2] = 92 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 1 ;
  n20->valeurs[0] = 59 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n5->enfants[1] = n21 ;
  Noeud* n22 = new Noeud(0) ;
  n22->taille = 2 ;
  n22->valeurs[0] = 78 ;
  n22->valeurs[1] = 82 ;
  n5->enfants[2] = n22 ;
  Noeud* n23 = new Noeud(0) ;
  n23->taille = 2 ;
  n23->valeurs[0] = 93 ;
  n23->valeurs[1] = 97 ;
  n5->enfants[3] = n23 ;
  n1->enfants[1] = n5 ;
  n1->enfants[2] = nullptr ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_17() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 10 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 3 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 28 ;
  n17->valeurs[2] = 44 ;
  n4->enfants[1] = n17 ;
  n4->enfants[2] = nullptr ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 1 ;
  n20->valeurs[0] = 59 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 1 ;
  n6->valeurs[0] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 3 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 82 ;
  n24->valeurs[2] = 85 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 2 ;
  n25->valeurs[0] = 93 ;
  n25->valeurs[1] = 97 ;
  n6->enfants[1] = n25 ;
  n6->enfants[2] = nullptr ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_18() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 10 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 3 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 28 ;
  n17->valeurs[2] = 44 ;
  n4->enfants[1] = n17 ;
  n4->enfants[2] = nullptr ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 2 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 59 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 1 ;
  n6->valeurs[0] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 3 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 82 ;
  n24->valeurs[2] = 85 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 2 ;
  n25->valeurs[0] = 93 ;
  n25->valeurs[1] = 97 ;
  n6->enfants[1] = n25 ;
  n6->enfants[2] = nullptr ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_19() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 10 ;
  n4->valeurs[1] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 27 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 1 ;
  n18->valeurs[0] = 44 ;
  n4->enfants[2] = n18 ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 2 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 59 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 1 ;
  n6->valeurs[0] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 3 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 82 ;
  n24->valeurs[2] = 85 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 2 ;
  n25->valeurs[0] = 93 ;
  n25->valeurs[1] = 97 ;
  n6->enfants[1] = n25 ;
  n6->enfants[2] = nullptr ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_20() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 10 ;
  n4->valeurs[1] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 27 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 1 ;
  n18->valeurs[0] = 44 ;
  n4->enfants[2] = n18 ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 2 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 59 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 82 ;
  n6->valeurs[1] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 2 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 80 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 85 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 2 ;
  n26->valeurs[0] = 93 ;
  n26->valeurs[1] = 97 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_21() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 10 ;
  n4->valeurs[1] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 27 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 2 ;
  n18->valeurs[0] = 41 ;
  n18->valeurs[1] = 44 ;
  n4->enfants[2] = n18 ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 2 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 59 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 82 ;
  n6->valeurs[1] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 2 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 80 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 85 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 2 ;
  n26->valeurs[0] = 93 ;
  n26->valeurs[1] = 97 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_22() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 10 ;
  n4->valeurs[1] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 27 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 2 ;
  n18->valeurs[0] = 41 ;
  n18->valeurs[1] = 44 ;
  n4->enfants[2] = n18 ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 3 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 59 ;
  n20->valeurs[2] = 62 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 82 ;
  n6->valeurs[1] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 2 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 80 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 85 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 2 ;
  n26->valeurs[0] = 93 ;
  n26->valeurs[1] = 97 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_23() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 10 ;
  n4->valeurs[1] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 27 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 2 ;
  n18->valeurs[0] = 41 ;
  n18->valeurs[1] = 44 ;
  n4->enfants[2] = n18 ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 3 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 59 ;
  n20->valeurs[2] = 62 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 3 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n21->valeurs[2] = 75 ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 82 ;
  n6->valeurs[1] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 2 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 80 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 85 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 2 ;
  n26->valeurs[0] = 93 ;
  n26->valeurs[1] = 97 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_24() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 10 ;
  n4->valeurs[1] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 2 ;
  n16->valeurs[0] = 4 ;
  n16->valeurs[1] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 27 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 2 ;
  n18->valeurs[0] = 41 ;
  n18->valeurs[1] = 44 ;
  n4->enfants[2] = n18 ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 3 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 59 ;
  n20->valeurs[2] = 62 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 3 ;
  n21->valeurs[0] = 71 ;
  n21->valeurs[1] = 73 ;
  n21->valeurs[2] = 75 ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 82 ;
  n6->valeurs[1] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 2 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 80 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 85 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 2 ;
  n26->valeurs[0] = 93 ;
  n26->valeurs[1] = 97 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_25() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 10 ;
  n4->valeurs[1] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 2 ;
  n16->valeurs[0] = 4 ;
  n16->valeurs[1] = 6 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 27 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 2 ;
  n18->valeurs[0] = 41 ;
  n18->valeurs[1] = 44 ;
  n4->enfants[2] = n18 ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 59 ;
  n5->valeurs[1] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 2 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 53 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 1 ;
  n21->valeurs[0] = 62 ;
  n5->enfants[1] = n21 ;
  Noeud* n22 = new Noeud(0) ;
  n22->taille = 3 ;
  n22->valeurs[0] = 71 ;
  n22->valeurs[1] = 73 ;
  n22->valeurs[2] = 75 ;
  n5->enfants[2] = n22 ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 82 ;
  n6->valeurs[1] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 2 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 80 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 85 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 2 ;
  n26->valeurs[0] = 93 ;
  n26->valeurs[1] = 97 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_26() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 2 ;
  n4->valeurs[0] = 10 ;
  n4->valeurs[1] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 3 ;
  n16->valeurs[0] = 4 ;
  n16->valeurs[1] = 6 ;
  n16->valeurs[2] = 10 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 24 ;
  n17->valeurs[1] = 27 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 2 ;
  n18->valeurs[0] = 41 ;
  n18->valeurs[1] = 44 ;
  n4->enfants[2] = n18 ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 59 ;
  n5->valeurs[1] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 2 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 53 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 1 ;
  n21->valeurs[0] = 62 ;
  n5->enfants[1] = n21 ;
  Noeud* n22 = new Noeud(0) ;
  n22->taille = 3 ;
  n22->valeurs[0] = 71 ;
  n22->valeurs[1] = 73 ;
  n22->valeurs[2] = 75 ;
  n5->enfants[2] = n22 ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 82 ;
  n6->valeurs[1] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 2 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 80 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 85 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 2 ;
  n26->valeurs[0] = 93 ;
  n26->valeurs[1] = 97 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_27() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 3 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 10 ;
  n4->valeurs[2] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 4 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 7 ;
  n17->valeurs[1] = 10 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 2 ;
  n18->valeurs[0] = 24 ;
  n18->valeurs[1] = 27 ;
  n4->enfants[2] = n18 ;
  Noeud* n19 = new Noeud(0) ;
  n19->taille = 2 ;
  n19->valeurs[0] = 41 ;
  n19->valeurs[1] = 44 ;
  n4->enfants[3] = n19 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 59 ;
  n5->valeurs[1] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 2 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 53 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 1 ;
  n21->valeurs[0] = 62 ;
  n5->enfants[1] = n21 ;
  Noeud* n22 = new Noeud(0) ;
  n22->taille = 3 ;
  n22->valeurs[0] = 71 ;
  n22->valeurs[1] = 73 ;
  n22->valeurs[2] = 75 ;
  n5->enfants[2] = n22 ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 82 ;
  n6->valeurs[1] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 2 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 80 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 85 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 2 ;
  n26->valeurs[0] = 93 ;
  n26->valeurs[1] = 97 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_28() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 2 ;
  n1->valeurs[0] = 45 ;
  n1->valeurs[1] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 3 ;
  n4->valeurs[0] = 6 ;
  n4->valeurs[1] = 10 ;
  n4->valeurs[2] = 28 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 4 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 7 ;
  n17->valeurs[1] = 10 ;
  n4->enfants[1] = n17 ;
  Noeud* n18 = new Noeud(0) ;
  n18->taille = 2 ;
  n18->valeurs[0] = 24 ;
  n18->valeurs[1] = 27 ;
  n4->enfants[2] = n18 ;
  Noeud* n19 = new Noeud(0) ;
  n19->taille = 2 ;
  n19->valeurs[0] = 41 ;
  n19->valeurs[1] = 44 ;
  n4->enfants[3] = n19 ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 2 ;
  n5->valeurs[0] = 59 ;
  n5->valeurs[1] = 67 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 3 ;
  n20->valeurs[0] = 46 ;
  n20->valeurs[1] = 53 ;
  n20->valeurs[2] = 56 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 1 ;
  n21->valeurs[0] = 62 ;
  n5->enfants[1] = n21 ;
  Noeud* n22 = new Noeud(0) ;
  n22->taille = 3 ;
  n22->valeurs[0] = 71 ;
  n22->valeurs[1] = 73 ;
  n22->valeurs[2] = 75 ;
  n5->enfants[2] = n22 ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 82 ;
  n6->valeurs[1] = 92 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 2 ;
  n24->valeurs[0] = 78 ;
  n24->valeurs[1] = 80 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 85 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 2 ;
  n26->valeurs[0] = 93 ;
  n26->valeurs[1] = 97 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* solution_insertion_29() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 3 ;
  n1->valeurs[0] = 10 ;
  n1->valeurs[1] = 45 ;
  n1->valeurs[2] = 75 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 6 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 4 ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 2 ;
  n17->valeurs[0] = 7 ;
  n17->valeurs[1] = 10 ;
  n4->enfants[1] = n17 ;
  n4->enfants[2] = nullptr ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 28 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 2 ;
  n20->valeurs[0] = 24 ;
  n20->valeurs[1] = 27 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 3 ;
  n21->valeurs[0] = 41 ;
  n21->valeurs[1] = 44 ;
  n21->valeurs[2] = 45 ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  Noeud* n6 = new Noeud(0) ;
  n6->taille = 2 ;
  n6->valeurs[0] = 59 ;
  n6->valeurs[1] = 67 ;
  Noeud* n24 = new Noeud(0) ;
  n24->taille = 3 ;
  n24->valeurs[0] = 46 ;
  n24->valeurs[1] = 53 ;
  n24->valeurs[2] = 56 ;
  n6->enfants[0] = n24 ;
  Noeud* n25 = new Noeud(0) ;
  n25->taille = 1 ;
  n25->valeurs[0] = 62 ;
  n6->enfants[1] = n25 ;
  Noeud* n26 = new Noeud(0) ;
  n26->taille = 3 ;
  n26->valeurs[0] = 71 ;
  n26->valeurs[1] = 73 ;
  n26->valeurs[2] = 75 ;
  n6->enfants[2] = n26 ;
  n6->enfants[3] = nullptr ;
  n1->enfants[2] = n6 ;
  Noeud* n7 = new Noeud(0) ;
  n7->taille = 2 ;
  n7->valeurs[0] = 82 ;
  n7->valeurs[1] = 92 ;
  Noeud* n28 = new Noeud(0) ;
  n28->taille = 2 ;
  n28->valeurs[0] = 78 ;
  n28->valeurs[1] = 80 ;
  n7->enfants[0] = n28 ;
  Noeud* n29 = new Noeud(0) ;
  n29->taille = 1 ;
  n29->valeurs[0] = 85 ;
  n7->enfants[1] = n29 ;
  Noeud* n30 = new Noeud(0) ;
  n30->taille = 2 ;
  n30->valeurs[0] = 93 ;
  n30->valeurs[1] = 97 ;
  n7->enfants[2] = n30 ;
  n7->enfants[3] = nullptr ;
  n1->enfants[3] = n7 ;

  return n1 ;
}

Noeud* solution_insertion_30() {
  Noeud* n1 = new Noeud(0) ;
  n1->taille = 1 ;
  n1->valeurs[0] = 45 ;
  Noeud* n4 = new Noeud(0) ;
  n4->taille = 1 ;
  n4->valeurs[0] = 10 ;
  Noeud* n16 = new Noeud(0) ;
  n16->taille = 1 ;
  n16->valeurs[0] = 6 ;
  Noeud* n64 = new Noeud(0) ;
  n64->taille = 1 ;
  n64->valeurs[0] = 4 ;
  n16->enfants[0] = n64 ;
  Noeud* n65 = new Noeud(0) ;
  n65->taille = 2 ;
  n65->valeurs[0] = 7 ;
  n65->valeurs[1] = 10 ;
  n16->enfants[1] = n65 ;
  n16->enfants[2] = nullptr ;
  n16->enfants[3] = nullptr ;
  n4->enfants[0] = n16 ;
  Noeud* n17 = new Noeud(0) ;
  n17->taille = 1 ;
  n17->valeurs[0] = 28 ;
  Noeud* n68 = new Noeud(0) ;
  n68->taille = 2 ;
  n68->valeurs[0] = 24 ;
  n68->valeurs[1] = 27 ;
  n17->enfants[0] = n68 ;
  Noeud* n69 = new Noeud(0) ;
  n69->taille = 3 ;
  n69->valeurs[0] = 41 ;
  n69->valeurs[1] = 44 ;
  n69->valeurs[2] = 45 ;
  n17->enfants[1] = n69 ;
  n17->enfants[2] = nullptr ;
  n17->enfants[3] = nullptr ;
  n4->enfants[1] = n17 ;
  n4->enfants[2] = nullptr ;
  n4->enfants[3] = nullptr ;
  n1->enfants[0] = n4 ;
  Noeud* n5 = new Noeud(0) ;
  n5->taille = 1 ;
  n5->valeurs[0] = 75 ;
  Noeud* n20 = new Noeud(0) ;
  n20->taille = 3 ;
  n20->valeurs[0] = 53 ;
  n20->valeurs[1] = 59 ;
  n20->valeurs[2] = 67 ;
  Noeud* n80 = new Noeud(0) ;
  n80->taille = 1 ;
  n80->valeurs[0] = 46 ;
  n20->enfants[0] = n80 ;
  Noeud* n81 = new Noeud(0) ;
  n81->taille = 2 ;
  n81->valeurs[0] = 54 ;
  n81->valeurs[1] = 56 ;
  n20->enfants[1] = n81 ;
  Noeud* n82 = new Noeud(0) ;
  n82->taille = 1 ;
  n82->valeurs[0] = 62 ;
  n20->enfants[2] = n82 ;
  Noeud* n83 = new Noeud(0) ;
  n83->taille = 3 ;
  n83->valeurs[0] = 71 ;
  n83->valeurs[1] = 73 ;
  n83->valeurs[2] = 75 ;
  n20->enfants[3] = n83 ;
  n5->enfants[0] = n20 ;
  Noeud* n21 = new Noeud(0) ;
  n21->taille = 2 ;
  n21->valeurs[0] = 82 ;
  n21->valeurs[1] = 92 ;
  Noeud* n84 = new Noeud(0) ;
  n84->taille = 2 ;
  n84->valeurs[0] = 78 ;
  n84->valeurs[1] = 80 ;
  n21->enfants[0] = n84 ;
  Noeud* n85 = new Noeud(0) ;
  n85->taille = 1 ;
  n85->valeurs[0] = 85 ;
  n21->enfants[1] = n85 ;
  Noeud* n86 = new Noeud(0) ;
  n86->taille = 2 ;
  n86->valeurs[0] = 93 ;
  n86->valeurs[1] = 97 ;
  n21->enfants[2] = n86 ;
  n21->enfants[3] = nullptr ;
  n5->enfants[1] = n21 ;
  n5->enfants[2] = nullptr ;
  n5->enfants[3] = nullptr ;
  n1->enfants[1] = n5 ;
  n1->enfants[2] = nullptr ;
  n1->enfants[3] = nullptr ;

  return n1 ;
}

Noeud* (*solution_insertion[30])() = {
  solution_insertion_1,
  solution_insertion_2,
  solution_insertion_3,
  solution_insertion_4,
  solution_insertion_5,
  solution_insertion_6,
  solution_insertion_7,
  solution_insertion_8,
  solution_insertion_9,
  solution_insertion_10,
  solution_insertion_11,
  solution_insertion_12,
  solution_insertion_13,
  solution_insertion_14,
  solution_insertion_15,
  solution_insertion_16,
  solution_insertion_17,
  solution_insertion_18,
  solution_insertion_19,
  solution_insertion_20,
  solution_insertion_21,
  solution_insertion_22,
  solution_insertion_23,
  solution_insertion_24,
  solution_insertion_25,
  solution_insertion_26,
  solution_insertion_27,
  solution_insertion_28,
  solution_insertion_29,
  solution_insertion_30
} ;

Noeud* exemple_recherche() {
  return solution_insertion_30() ;
}
