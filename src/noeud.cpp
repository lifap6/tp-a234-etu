#include "noeud.hpp"

#include <iostream>
#include <sstream>
#include <cassert>

Noeud::Noeud(int v) {
  //votre initialisation ici
}

Noeud::~Noeud() {
  //votre destruction ici
}

void Noeud::eclater(int index) {
  //votre code ici
}

void Noeud::inserer(int v) {
  //votre code ici
}

Noeud* Noeud::rechercher(int v) {
  //votre code ici
  //vous pouvez supprimer la ligne qui suit
  return nullptr ;
}

static void afficher(std::ostream& out, Noeud* n, int profondeur) {
  //la profondeur indique l'indentation avec laquelle afficher
  if(n) {
    //affichage du premier enfant
    afficher(out, n->enfants[n->taille], profondeur + 1) ;
    //intercalement des valeurs et des enfants suivants
    for(int i = n->taille - 1; i >= 0; --i) {
      //indentation
      for(int j = 0; j < profondeur; ++j) {
        out << "|  " ;
      }
      //affichage de la valeur suivante
      out << "+ " << n->valeurs[i] << std::endl ;
      //affichage de l'enfant au dessus de cette valeur
      afficher(out, n->enfants[i], profondeur + 1) ;
    }
  }
}

std::ostream& operator<<(std::ostream& out, Noeud* n) {
  afficher(out, n, 0) ;
  return out ;
}

//verification de l'egalite de deux noeuds
void test_egal(Noeud* n1, Noeud* n2) {
  if(n1) {
    //si n1 existe, n2 doit aussi exister
    assert(n2 && "second noeud manquant") ;
    //les tailles doivent etre les memes
    assert(n1->taille == n2->taille && "tailles differentes") ;
    //les valeurs doivent etre les memes
    for(int i = 0; i < n1->taille; ++i) {
      assert(n1->valeurs[i] == n2->valeurs[i] && "valeurs differentes") ;
    }
    //les enfants doivent etre egaux
    for(int i = 0; i <= n1->taille; ++i) {
      test_egal(n1->enfants[i], n2->enfants[i]) ;
    }
  } else {
    //si n1 n'existe pas, n2 ne doit pas non-plus exister
    assert(!n2 && "premier noeud manquant") ;
  }
}
