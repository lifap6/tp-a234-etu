#include "noeud.hpp"

//test recherche
Noeud* exemple_recherche() ;

//test eclatement
Noeud* exemple_eclatement() ;
Noeud* solution_eclatement_1() ;
Noeud* solution_eclatement_2() ;

//test_insertion
extern Noeud* (*solution_insertion[30])() ;
