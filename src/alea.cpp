#include "alea.hpp"

Alea::Alea() : state(0) {}

//https://en.wikipedia.org/wiki/Linear_congruential_generator
int Alea::entier() {
  unsigned int a =	1103515245 ;
  unsigned int c = 12345 ;
  state = (a * state + c) % (1 << 31) ;
  return state % 100 ;
}
