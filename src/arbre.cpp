#include "arbre.hpp"
#include "exemple.hpp"

#include <iostream>

Arbre::Arbre() : racine(nullptr) {}

Arbre::~Arbre() {
  //votre destruction ici
}

void Arbre::inserer(int v) {
  //votre code ici
}

Noeud* Arbre::rechercher(int v) {
  //votre code ici
}

std::ostream& operator<<(std::ostream& out, Arbre& a) {
  //afficher la racine
  out << a.racine ;
  return out ;
}
