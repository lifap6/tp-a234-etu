#ifndef A234_NOEUD_HPP
#define A234_NOEUD_HPP

#include <iostream>

struct Noeud {
  Noeud(int v) ;
  ~Noeud() ;

  //insertino d'une valeur
  void inserer(int v) ;

  //eclatement d'un enfant
  void eclater(int index) ;

  //recherche d'une valeur, renvoie le noeud descendant la contenant
  Noeud* rechercher(int v) ;

  //variable membres

  //taille : un entier
  //valeurs : un tableau de trois entiers
  //enfants : un tableau de quatre adresses de noeuds
} ;

//affichage
std::ostream& operator<<(std::ostream& out, Noeud* n) ;

//pour tester l'egalite de deux arbres
void test_egal(Noeud* n1, Noeud* n2) ;

#endif
