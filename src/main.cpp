//#define A234_STRUCTURE_BASE
//#define A234_RECHERCHE
//#define A234_ECLATEMENT
//#define A234_INSERTION_0
//#define A234_INSERTION_FEUILLE
//#define A234_ECLATEMENT_RACINE
//#define A234_INSERTION

#include "arbre.hpp"
#include "alea.hpp"
#include "exemple.hpp"

#include <iostream>
#include <vector>
#include <cassert>

int main() {
  
  //nombres aleatoires
  Alea alea ;

  //valeurs a inserer
  std::vector<int> valeurs ;

  for(int i = 0; i < 30; ++i) {
    //tirer un nombre a inserer, l'enregistrer
    valeurs.push_back(alea.entier()) ;
  }

  //valeurs absentes
  std::vector<int> absentes({2, 12, 30, 50, 60, 70, 76, 81, 90, 100}) ;

#ifdef A234_STRUCTURE_BASE
  //test de la structure de base
  {
    std::cout << "===== Test structure de base ====" << std::endl ;
    //initialisation d'un exemple
    Arbre a ;
    a.racine = exemple_recherche() ;

    std::cout << a ;

    /* Affichage attendu :
     *
     * |  |  |  + 97
     * |  |  |  + 93
     * |  |  + 92
     * |  |  |  + 85
     * |  |  + 82
     * |  |  |  + 80
     * |  |  |  + 78
     * |  + 75
     * |  |  |  + 75
     * |  |  |  + 73
     * |  |  |  + 71
     * |  |  + 67
     * |  |  |  + 62
     * |  |  + 59
     * |  |  |  + 56
     * |  |  |  + 54
     * |  |  + 53
     * |  |  |  + 46
     * + 45
     * |  |  |  + 45
     * |  |  |  + 44
     * |  |  |  + 41
     * |  |  + 28
     * |  |  |  + 27
     * |  |  |  + 24
     * |  + 10
     * |  |  |  + 10
     * |  |  |  + 7
     * |  |  + 6
     * |  |  |  + 4
     */
    std::cout << "OK." << std::endl ;
  }
#endif

#ifdef A234_RECHERCHE
  //test de la recherche
  {
    std::cout << "===== Test recherche ====" << std::endl ;
    //initialisation de l'exemple
    Arbre a ;
    a.racine = exemple_recherche() ;

    std::cout << "\n=== valeurs presentes\n\n" ;

    //verification de la presence des valeurs inserees
    for(int valeur : valeurs) {
      std::cout << "recherche de " << valeur << " ... " ;
      assert(a.rechercher(valeur) && "une valeur inseree n'est pas retrouvee") ;
      std::cout << "trouvee.\n" ;
    }

    std::cout << "\n=== valeurs absentes\n\n" ;

    //verification de l'absence des valeurs absentes
    for(int valeur : absentes) {
      std::cout << "recherche de " << valeur << " ... " ;
      assert(!a.rechercher(valeur) && "une valeur absente est retrouvee") ;
      std::cout << "absente.\n" ;
    }
    std::cout << "\nOK." << std::endl ;
  }
#endif

#ifdef A234_ECLATEMENT
  //test eclatement
  {
    std::cout << "===== Test eclatement ====" << std::endl ;
    //etat initial et solutions
    Noeud* n = exemple_eclatement() ;
    Noeud* s1 = solution_eclatement_1() ;
    Noeud* s2 = solution_eclatement_2() ;

    std::cout << std::endl << n ;

    //eclatement de l'enfant 0
    n->eclater(0) ;
    test_egal(n, s1) ;
    std::cout << std::endl << "=== eclatement de [10|12|14]\n" << std::endl ;
    std::cout << n ;

    //eclatement de l'enfant 1
    n->eclater(2) ;
    test_egal(n, s2) ;
    std::cout << std::endl << "=== eclatement de [30|32|34]\n" << std::endl ;
    std::cout << n ;

    //menage
    delete n ;
    delete s1 ;
    delete s2 ;
    std::cout << "\nOK." << std::endl ;
  }
#endif

#ifdef A234_INSERTION_0
  //test insertion
  {
    std::cout << "===== Test insertion premiere valeur ====" << std::endl ;
    Arbre a ;

    std::cout << "\ninsertion de " << valeurs[0] << " dans un arbre vide\n\n" ;

    //insertion dans l'arbre
    a.inserer(valeurs[0]) ;

    //reference
    Noeud* s = solution_insertion[0]() ;
    test_egal(a.racine, s) ;

    std::cout << a ;

    delete s ;
    std::cout << "\nOK." << std::endl ;
  }
#endif

#ifdef A234_INSERTION_FEUILLE
  //test insertion
  {
    std::cout << "===== Test insertion feuille ====" << std::endl ;
    Arbre a ;

    for(int i = 0; i < 3; ++i) {
      std::cout << "\ninsertion de " << valeurs[i] << " dans la racine de taille < 3\n\n" ;
      //insertion dans l'arbre
      a.inserer(valeurs[i]) ;

      std::cout << a ;

      //reference
      Noeud* s = solution_insertion[i]() ;
      test_egal(a.racine, s) ;

      delete s ;
    }
    std::cout << "\nOK." << std::endl ;
  }
#endif

#ifdef A234_ECLATEMENT_RACINE
  //test insertion
  {
    std::cout << "===== Test eclatement racine ====" << std::endl ;
    Arbre a ;

    std::cout << "\nremplissage de la racine \n\n" ;
    for(int i = 0; i < 3; ++i) {
      //insertion dans l'arbre
      a.inserer(valeurs[i]) ;

      //reference
      Noeud* s = solution_insertion[i]() ;
      test_egal(a.racine, s) ;
      delete s ;
    }

    std::cout << a ;

    std::cout << "\neclatement de la racine \n\n" ;
    {
      //insertion dans l'arbre
      a.inserer(valeurs[3]) ;

      //reference
      Noeud* s = solution_insertion[3]() ;
      test_egal(a.racine, s) ;

      std::cout << a ;

      delete s ;
    }
    
    std::cout << "\ndescente dans les feuilles \n\n" ;

    for(int i = 4; i < 6; ++i) {
      //insertion dans l'arbre
      a.inserer(valeurs[i]) ;

      //reference
      Noeud* s = solution_insertion[i]() ;
      test_egal(a.racine, s) ;
      delete s ;
    }

    std::cout << a ;

    std::cout << "\nOK." << std::endl ;
  }
#endif

#ifdef A234_INSERTION
  //test insertion
  {
    std::cout << "===== Test insertion ====" << std::endl ;
    Arbre a ;

    for(int i = 0; i < valeurs.size(); ++i) {
      std::cout << "\ninsertion de " << valeurs[i] << "\n\n" ;
      //insertion dans l'arbre
      a.inserer(valeurs[i]) ;

      std::cout << a ;

      //reference
      Noeud* s = solution_insertion[i]() ;
      test_egal(a.racine, s) ;

      delete s ;
    }
    std::cout << "\nOK." << std::endl ;
  }
#endif

  return 0 ;
}
