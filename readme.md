# Arbres 234

## Introduction

Le but de ce TP est l'implémentation des arbres 234. Ces arbres appartiennent à
la famille des arbres auto équilibrants. À la différence des AVL et des arbres
rouges noirs, ces arbres ne sont pas binaires : chaque nœud peut contenir entre
une et trois valeurs, et entre deux et quatre enfants. Cette liberté
supplémentaire permet de maintenir des arbres parfaitement équilibrés : toutes
les feuilles sont au même niveau.

Après une explication plus détaillée de la structure de données, votre but sera
d'implémenter la recherche et l'insertion de valeurs dans un arbre 234, ainsi
que la destruction.

### Principe des arbres 234

Dans un arbre 234, chaque nœud peut contenir entre une et trois valeurs. Le
nombre d'enfants est un de plus que le nombre de valeurs. Par exemple un nœud à
deux valeurs aura trois enfants, un nœud à trois valeurs aura quatre enfants.

Imaginons un nœud contenant les trois valeurs 10, 20 et 30. Il aura alors
quatre enfants : le premier ainsi que ses descendants ne contiendra que des
valeurs plus petites que 10, le second et sa descendance des valeurs entre 10 et
20, le troisième et sa descendance des valeurs entre 20 et 30 et le dernier et
sa descendance des valeurs plus grandes que 30. Lorsque le nœud contient moins
de valeurs, il y a moins d'enfants selon le même principe. Au minimum avec une 
seule valeur, le premier enfant et sa descendance sont plus petits que la valeur, 
et le second et sa descendance sont plus grands. On retrouve alors des arbres
binaires de recherche classiques.

<center>
  <img alt="noeud" src="img/noeud.svg" width=800px></img>
</center>

Cette liberté sur la taille des nœuds et le nombre d'enfant permet de maintenir
des arbres parfaitement équilibrés au fur et à mesure des insertions.


### Votre travail

Votre travail consiste à implémenter les arbres 234. Une base de code vous
permet leur affichage et la réalisation de tests pour les fonctionnalités à
développer. Dans ce TP, vous devrez dans l'ordre :

* ajouter des données dans la classe nœud pour gérer ses valeurs et enfants
* implémenter la recherche dans un arbre 234
* implémenter l'éclatement d'un nœud
* implémenter l'insertion
* implémenter la destruction

Votre travail est à rendre sous forme d'**une archive `tar.gz` ou `zip` via
TOMUSS**. Ce travail est individuel, à rendre à la fin du TP. Vous avez le droit
d'accéder au web pour vous documenter, mais pas de communiquer entre vous, et
votre code doit rester personnel (pas de copier coller de codes en ligne).

### Code fourni

Vous pouvez récupérer le code de base en clonant ce dépôt sur votre machine. Le
code de base est dans le dossier `src` avec un `Makefile`. **Ne forkez pas le
dépôt publiquement et ne poussez pas votre travail publiquement sur votre
fork**. Les fichiers de base contiennent un peu de code pour vous aider. Le code
ne compile pas de base, c'est normal, il vous faut commencer par compléter les
variables membres de la classe `Noeud`.

* `main.cpp` contient un ensemble de tests à activer au fur et à mesure
* `alea.[hc]pp` assure que vous avez toustes le même hasard
* `arbre.[hc]pp` fournit la base de la structure `Arbre`
* `noeud.[hc]pp` fournit la base de la structure `Noeud`
* `exemple.[hc]pp` fournit des arbres exemples préconçus pour les tests

Pour que ce code fonctionne, **vous devez respecter les noms proposés pour les 
variables membres la classe `Noeud`**, ou corriger les fonctions d'affichage et
tous les exemples pour qu'ils soient compatibles avec vos choix.

## Structure de base

La base de code fournie permet l'affichage et la construction d'arbres exemples
pour tester les fonctionnalités de votre code. Pour que ces fonctionnalités
fonctionnent, vous **devez** respecter les noms de variables proposés.

Un nœud doit contenir entre une et trois valeurs. Ces valeurs seront stockées
triées de la plus petite à la plus grande dans un **tableau** de trois 
entiers appelé `valeurs`. Pour savoir combien de valeurs sont stockées dans le
nœud, vous ajouterez **un entier** appelé `taille`. Enfin les enfants seront
stockés dans un **tableau** de 4 adresses de nœuds. En fonction de la valeur de
la variable `taille`, toutes les cases ne seront pas toujours utilisées. Nous
vous conseillons néanmoins d'initialiser vos tableaux. Vous pouvez utiliser `-1`
pour les valeurs non utilisées, car les tests n'utilisent que des valeurs
positives, et `nullptr` pour les enfants non utilisés.

Ci-dessous, un nœud de taille 2 contenant les valeurs 10 et 20. La taille est
stockée dans le nœud comme un entier, et les deux valeurs sont placées triées
dans un tableau de trois entiers. Le tableau ne contenant que deux valeurs
utiles, la dernière valeur non utilisée est initialisée à -1 pour aider au debug
éventuel. Les trois enfants sont stockés dans l'ordre dans un tableau de quatre
adresses de nœuds. De même seuls trois enfants sont utiles, le quatrième est
donc initialisé à `nullptr` pour aider au debug éventuel.

<center>
  <img alt="nœud" src="img/structure_noeud.svg" width=800px></img>
</center>

En respectant ces conventions, vous pouvez activer le premier test dans
`main.cpp` en décommentant la ligne `#define A234_STRUCTURE_BASE` en haut du
fichier. Si votre programme compile, ne plante pas, et affiche l'arbre attendu,
vous pouvez raisonnablement estimer que cette partie est résolue.

## Recherche

Avant de passer à l'insertion, pour vous assurer d'avoir compris la structure
des arbres 234, vous implémenterez la recherche. Vous remplirez pour cela les
fonctions `rechercher` dans les fichiers `arbre.cpp` et `nœud.cpp`. Pour
activer le test de votre fonction recherche, décommentez la ligne `#define
A234_RECHERCHE` en haut du fichier `main.cpp`.

La recherche prend en paramètre une valeur entière à chercher, et renvoie
l'adresse du nœud contenant cette valeur s'il existe. Si le nœud n'existe pas,
la fonction renvoie `nullptr`. Dans le code de base elle renvoie
systématiquement `nullptr`, c'est à vous de modifier ce comportement pour
trouver et renvoyer l'adresse du nœud contenant la valeur s'il existe.

Au niveau de l'arbre, la fonction recherche se contente d'appeler la fonction
recherche de la racine. C'est la fonction recherche des nœuds qui fait le
travail. Cette fonction commence par vérifier si le nœud contient la valeur
recherchée. Si c'est le cas, l'adresse du nœud est renvoyée. Sinon, il faut
identifier l'enfant dans lequel pourrait se trouver la valeur recherchée. Si la
valeur cherchée est plus petite que la première valeur stockée dans le nœud, il
faut aller chercher dans le premier enfant. Si elle est plus grande que la
première et plus petite que la seconde, il faut chercher dans le second enfant,
et ainsi de suite. Si la valeur cherchée est plus grande que la dernière valeur,
il faut chercher dans le dernier enfant. Si l'enfant n'existe pas (vaut
`nullptr`), alors la recherche est terminée et la valeur cherchée n'est pas dans
l'arbre. Vous pouvez donc renvoyer `nullptr`. Si lors de votre recherche de
l'enfant vous trouvez une valeur égale à celle recherchée, pas besoin de
chercher plus bas, vous avez trouvé un nœud contenant la valeur (le nœud
courant), vous pouvez renvoyez son adresse (qu'on peut obtenir via la variable `this`).

**Attention** : lorsque vous appelez la fonction recherche sur un enfant, il est
nécessaire de récupérer sa valeur de retour **et de la renvoyer**.

Par exemple sur l'arbre ci-dessous
<center>
  <img alt="nœud" src="img/example_tree.svg" width = 800></img>
</center>

Une recherche de **59** passerait par les étapes suivantes :
* **sur [25] :** 59 > 25 et 25 est la dernière valeur de la racine => recherche dans
  l'enfant 1
* **sur [32|57|62] :** 59 > 32, 59 > 57 et 59 < 62 => recherche dans l'enfant 2
* **sur [58|59|60] :** 59 > 58, 59 == 59 => la valeur est trouvée
* **sur [58|59|60] :** l'adresse de [58|59|60] est retournée

Une recherche de **15** passerait par les étapes suivantes :
* **sur [25] :** 15 < 25 et 25 est la première valeur de la racine => recherche dans 
  l'enfant 0
* **sur [7|21] :** 15 > 7 et 15 < 21 => recherche dans l'enfant 1
* **sur [11|13|20] :** 15 > 11, 15 > 13 et 15 < 20, mais les enfants sont
  `nullptr`
* **sur [11|13|20] :** la valeur ne peut pas être trouvée, retour de `nullptr`

La fonction de test pour cette section charge un arbre exemple préconfiguré et
s'assure que votre recherche trouve toutes les valeurs, et ne trouve pas un
certain nombre de valeurs absentes.

## Éclatement

Si vous bloquez sur cette partie, vous n'avez pas besoin de l'éclatement pour
réaliser le début de l'insertion.

La seule façon d'ajouter des nœuds dans un arbre 234 est via l'éclatement.
L'éclatement peut être réalisé sur un nœud de taille 3. Lorsqu'un tel nœud est
éclaté, sa valeur du milieu est remontée dans son parent (en veillant à garder
les valeurs du parent triées). Le nœud est ensuite séparé en deux nœuds de
taille 1 avec les deux valeurs restantes, placés de part et d'autre de la valeur
remontée. Les quatre enfants du nœud sont répartis entre les deux nœuds de
taille 1 créés : les deux plus petits enfants formeront les deux enfants du
nœud de taille 1 contenant la plus petite valeur, et les deux plus grands
enfants iront dans le nœud de taille 1 contenant la plus grande valeur. Pour
que l'éclatement puisse se faire sans problème il faut que le nœud parent soit
de taille plus petite que trois pour pouvoir accueillir une nouvelle valeur.

<center>
  <img alt="nœud" src="img/eclatement.svg" width=500px></img>
</center>

Sur la portion d'arbre ci-dessus, un éclatement est réalisé sur le nœud
`[15|20|25]`. Le 20 est donc remonté dans le nœud parent `[10|30]` à sa place
entre 10 et 20 pour maintenir les valeurs triées. Les deux autres valeurs 15 et
25 forment désormais deux nœuds de taille 1, chacun reprenant deux des enfants
initiaux du nœud éclaté.

Complétez la fonction `eclater` dans `nœud.cpp` pour réaliser un éclatement.
Pour éviter d'avoir à stocker les parents des nœuds, la fonction éclatement est
déclenchée au niveau du parent pour éclater un de ses enfants. Ainsi en appelant
`n->eclater(1)`, le second enfant de `n` (d'indice 1) est éclaté, et sa valeur
du milieu est remontée dans `n`. On suppose donc pour cet appel que `n` est de
taille strictement plus petite que trois, et que `n->enfants[1]` est de taille
exactement trois.

Pour déclencher les tests de cette section, décommentez la ligne `#define
A234_ECLATEMENT` en haut de `main.cpp`.

## Insertion

Dans un arbre 234, les nouvelles valeurs sont systématiquement insérées **dans
les feuilles**. La seule façon de faire grandir les nœuds internes en leur
ajoutant des valeurs est d'éclater leurs enfants. Pour s'assurer que la feuille
dans laquelle on arrive a la place d'accueillir une valeur de plus, des
**éclatements préventifs** sont réalisés lors de la descente de l'arbre pour
trouver le point d'insertion : dès qu'un nœud de taille 3 est rencontré sur le
chemin, il est éclaté.

### Toute première insertion

Lors de l'insertion de la toute première valeur dans l'arbre, la racine est
`nullptr`, il faut donc créer un premier nœud de taille 1 pour contenir la
valeur donnée à insérer. Complétez la fonction `insertion` de `arbre.cpp` pour
ajouter un test vérifiant si nous sommes dans ce cas là, et pour gérer ce cas.
Lorsque la racine existe déjà, vous pouvez alors faire appel à sa méthode
`insertion` pour insérer la valeur donnée dans l'arbre. Nous nous occuperons de
cette méthode dans les parties suivantes.

Vous pouvez décommenter l'instruction `#define A234_INSERTION_0` en haut
de `main.cpp` pour tester ce point.

### Insertion dans les feuilles

Nous allons ensuite gérer le cas des feuilles. Une feuille se reconnaît car tous
ses enfants sont `nullptr` (il suffit normalement de tester l'enfant d'indice
0). Vous pouvez donc commencer à compléter la fonction `insertion` dans
`noeud.cpp` pour gérer le cas d'arrêt. Ajoutez un test pour déterminer si le
nœud sur lequel la méthode est appelée est une feuille, et pour traiter ce cas.

Dans le cas d'une feuille, la valeur insérée est ajoutée aux valeurs de la
feuille. Il est donc nécessaire que la feuille soit de taille strictement plus
petite que trois pour que ce soit possible. Les éclatement préventifs que nous
réaliserons par la suite s'en assureront. En supposant que la feuille a
suffisamment de place, lorsqu'on ajoute une valeur à un nœud, il faut s'assurer
que les valeurs restent triées. Faites donc le nécessaire pour déterminer à
quelle position la valeur doit être insérée dans le tableau, et pour décaler les
valeurs supérieures d'un cran pour faire de la place. N'oubliez pas d'augmenter
la taille du nœud.

Vous pouvez décommenter l'instruction `#define A234_INSERTION_FEUILLE` en haut
de `main.cpp` pour tester ce point. La racine étant initialement une feuille, il
est donc possible de ce contenter de ce cas tant que l'arbre contient moins de
trois valeurs.

### Éclatement de la racine et localisation de la feuille d'insertion

Après les trois premières insertion, quand la racine est de taille 3, il est
temps de commencer à faire pousser l'arbre en lui ajoutant des niveau. La seule
façon d'augmenter la hauteur d'un arbre 234 est d'en faire éclater la racine.

Vous avez normalement implémenté la méthode d'éclatement d'un nœud. Cette
méthode s'appelle sur le parent du nœud à éclater. La racine n'a cependant pas
de parent, mais quand elle est de taille 3, il devient tout de même nécessaire
de l'éclater. Sans ça, ses enfants ne peuvent plus être éclatés faute de place
pour faire remonter leur valeur du milieu. Ils se remplissent, puis leurs
enfants et ainsi de suite, l'arbre ne peut finalement plus grandir. 

Pour éclater la racine, un nouveau nœud de taille 1 est créé avec la valeur du
milieu de la racine. Ce nouveau nœud devient la nouvelle racine de l'arbre.
Comme enfants, cette nouvelle racine a deux nœuds de taille 1, correspondant aux
deux autres valeurs de la racine, la plus petite et la plus grande. Ces deux
nœuds de taille 1 ont respectivement pour enfants les deux premiers de
l'ancienne racine et les deux derniers.

<center>
  <img alt="nœud" src="img/eclatement_racine.svg" width=500px></img>
</center>

Pour réaliser cet éclatement, vous pouvez créer un nouveau nœud avec une valeur
bidon, mettre sa taille à 0 et renseigner l'actuelle racine comme premier
enfant. Vous devriez alors pouvoir appeler votre fonction `nouveau->eclater(0)`
pour éclater la racine, et remonter sa valeur centrale dans le nouveau nœud qui
devient valide de taille 1.

L'éclatement de la racine est à déclencher dans la méthode `insertion` de
`arbre.cpp`, lorsque l'insertion est appelée avec racine de taille 3. Dans ce
cas, éclatez la racine avant de procéder à l'insertion en appelant la méthode
d'insertion de la nouvelle racine. Vous pouvez ensuite procéder de façon
similaire à la recherche pour descendre dans l'arbre, arriver sur la feuille
dans laquelle insérer la valeur, et l'insérer dedans (en supposant que la
feuille est de taille plus petite que 3).

Vous pouvez décommenter l'instruction `#define A234_ECLATEMENT_RACINE` en haut
de `main.cpp` pour tester ce point. Ce test s'assure que les feuilles dans
lesquelles les valeurs sont insérées ne sont pas pleines.

### Éclatement préventif

Lorsque la feuille dans laquelle vous voulez insérer est pleine, il faut la
faire éclater pour obtenir des feuilles plus petites dans lesquelles insérer.
Cet éclatement fait remonter une valeur dans le parent de la feuille, mais s'il
est lui-même plein, il faudrait le faire éclater à son tour, ce qui peut
déclencher une réaction en chaîne compliquée.

Pour éviter les réaction en chaine, il est possible de réaliser des éclatements
**préventifs** : tous les nœuds de taille 3 rencontrés sur le chemin d'insertion
en descendant l'arbre sont éclatés au passage. Vu que les parents sont visités
avant les enfants, le nœud courant ne peut alors jamais être de taille 3, car si
ça avait été le cas, son parent l'aurait fait éclater avant d'y descendre.
Lorsqu'on arrive à une feuille, elle sera donc toujours de taille strictement
inférieure à 3.

Pour mettre en œuvre cet éclatement préventif, après avoir déterminé l'enfant
dans lequel poursuivre l'insertion, s'il est de taille 3, procédez à son
éclatement en appelant la méthode d'éclatement du nœud actuel. Cet éclatement
modifiera les valeurs et les enfants du nœud actuel, et il faudra donc ensuite
déterminer à nouveau l'enfant dans lequel poursuivre l'insertion. Cet enfant
sera de taille 1, et il est donc désormais possible d'y poursuivre l'insertion.

Par exemple sur l'arbre ci-dessous
<center>
  <img alt="nœud" src="img/eclatement_preventif.svg" width = 800></img>
</center>

L'insertion de la valeur 61 sur la racine [25] fait que l'enfant [32|57|62] est
sélectionné pour recevoir l'insertion. Cet enfant étant de taille 3, un
éclatement préventif est réalisé, faisant remonter la valeur 57 dans la racine.
Suite à cet éclatement, l'enfant [62] est sélectionné pour poursuivre
l'insertion. Une fois dans l'appel à l'insertion sur [62], l'enfant [58|59|60]
est sélectionné pour recevoir l'insertion. Ce nœud est également plein, et un
éclatement préventif est déclenché. Cet éclatement fait remonter la valeur 59
dans le nœud [62]. Suite à cet éclatement l'enfant [60] est sélectionné pour
poursuivre l'insertion. Une fois dans l'appel à l'insertion sur [60], étant
donné que [60] est une feuille, la valeur 61 est ajoutée dans les valeurs du
nœud.

Pour activer les tests de cette section, vous pouvez décommenter la ligne
`#define A234_INSERTION` en haut de `main.cpp`. Vous gérez normalement désormais
toutes les particularités de l'insertion. **Attention :** pour obtenir les mêmes
résultats que le jeu de test, lors de l'insertion d'une valeur déjà présente
dans l'arbre, cette valeur devra être insérée **du côté de l'enfant ayant des
valeurs plus petites que la valeur égale**.

## Destruction

Une fois vos arbres 234 fonctionnels, il vous reste à ajouter des destructeurs,
et à vous assurer de ne pas avoir de fuites mémoire dans vos fonctions. En
particulier, assurez vous que l'éclatement ne laisse pas des nœuds orphelins en
mémoire.
